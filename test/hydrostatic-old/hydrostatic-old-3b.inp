TWO-FLUID 2D HYDROSTATIC TEST
-----------------------------

Two inviscid fluids at rest in a [-5,5]^2 domain subject to gravity; lighter
fluid on top. Fluid should remain at rest, with a linear pressure field.

With g=(0,-1,0): rho2 = 2, p = c1 - 2*y, in y < 0;
                 rho1 = 1, p = c2 - y, in y > 0.

Variations use rotated meshes (and rotated g)

&OUTPUTS
  output_t  = 0.0, 2.0
  output_dt = 2.0
/

&MESH
  mesh_file = 'mesh1-rotx.gen'
/

&PHYSICS
  materials = 'water', 'oil'
  legacy_flow = .true.
  body_force_density = 0.0, -0.707106781, -0.707106781
/

&NUMERICS
  dt_constant = 0.125
  discrete_ops_type           = 'ortho'
/

&LEGACY_FLOW
  inviscid   = .true.
  projection_linear_solution  = 'projection'
  body_force_face_method      = .true.
  courant_number              = 0.5
/

&LINEAR_SOLVER
  name = 'projection'
  method = 'fgmres'
  preconditioning_method = 'ssor'
  convergence_criterion = 1.0e-12
  relaxation_parameter = 1.4
  preconditioning_steps = 2
  maximum_iterations = 200
/

# FREE SLIP BC ALL AROUND

Lower half
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 1
  material_name = 'water'
  velocity = 3*0.0
  temperature = 0.0
/

upper half
&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 2
  material_name = 'oil'
  velocity = 3*0.0
  temperature = 0.0
/

&MATERIAL
  name = 'water'
  is_fluid = T
  density = 2.0
  specific_heat = 1.0
/

&MATERIAL
  name = 'oil'
  is_fluid = T
  density = 1.0
  specific_heat = 1.0
/
