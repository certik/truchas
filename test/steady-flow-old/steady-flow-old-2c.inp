STEADY INVISCID 2-MATERIAL FLOW
-------------------------------
2D square domain (1 cell thick in z) with steady uniform flow directed along
the x-axis. Prescribed inflow velocity on the left and dirichlet pressure on
the right. Two fluids with planar interface orthogonal to the x-axis. Fluid
density ratio of 1000 with denser fluid trailing. Constant initial velocity
field consistent with the BC. Velocity should remain constant and the fluid
interface should be advected exactly across the domain.

This problem is analogous to a pure advection problem, except that instead
of a prescribed constant velocity, we solve for the velocity, expecting to
recovery that constant velocity.

Variations use rotated meshes (and rotated velocity)

&OUTPUTS
  output_t  = 0.0, .0625, 2.0
  output_dt = 0.0625, 2.0
/

&MESH
  mesh_file = 'mesh1a-roty.gen'
/

&PHYSICS
  materials = 'water', 'oil'
  legacy_flow = .true.
/

&NUMERICS
  dt_constant = 0.0625
  discrete_ops_type = 'ortho'
/

&LEGACY_FLOW
  inviscid = .true.
  courant_number = 0.5
  projection_linear_solution  = 'projection'
  volume_track_subcycles = 2
/

&LINEAR_SOLVER
  name                   = 'projection'
  method                 = 'fgmres'
  preconditioning_method = 'ssor'
  preconditioning_steps  = 2
  relaxation_parameter   = 1.4
  convergence_criterion  = 1.0e-10
  maximum_iterations     = 50
/

Inlet velocity
&BC
  surface_name = 'from mesh file'
  mesh_surface = 1
  bc_variable  = 'velocity'
  bc_type      = 'dirichlet'
  bc_value     = 2.82842712474619, 0.0, 2.82842712474619  ! 4/sqrt(2)
/

Outlet pressure
&BC
  surface_name = 'from mesh file'
  mesh_surface = 2
  bc_variable  = 'pressure'
  bc_type      = 'dirichlet'
  bc_value     = 0.0
/

&BODY
  surface_name = 'plane'
  axis = 'x'
  rotation_angle = 0, 45, 0
  translation_pt = -2.82842712474619, 0.0, -2.82842712474619  ! 4/sqrt(2)
  material_name = 'oil'
  velocity = 2.82842712474619, 0.0, 2.82842712474619  ! 4/sqrt(2)
  temperature = 0.0
/


&BODY
  surface_name = 'background'
  material_name = 'water'
  velocity = 2.82842712474619, 0.0, 2.82842712474619  ! 4/sqrt(2)
  temperature = 0.0
/

&MATERIAL
  name = 'water'
  is_fluid = T
  density = 1000.0
/

&MATERIAL
  name = 'oil'
  is_fluid = T
  density = 1.0
/
