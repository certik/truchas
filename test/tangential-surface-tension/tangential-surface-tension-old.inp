&MESH
  mesh_file = 'tangential-surface-tension.gen'
/

&OUTPUTS
  Output_T = 0.0, 10.0
  Output_Dt = 5.0 !1.0
/

&PHYSICS
  materials = 'liquid'
  heat_transport = .true.
  legacy_flow = .true.
/

&DIFFUSION_SOLVER
  abs_temp_tol       = 0.0
  rel_temp_tol       = 1.0e-3
  abs_enthalpy_tol   = 0.0
  rel_enthalpy_tol   = 1.0e-3
  nlk_tol            = 0.01
  nlk_preconditioner = 'hypre_amg'
  pc_amg_cycles      = 2
  verbose_stepping   = .true.
/

&LINEAR_SOLVER
  name                            = 'projection'
  method                          = 'fgmres'
  preconditioning_method          = 'ssor'
  convergence_criterion           = 1.0e-10
  relaxation_parameter            = 1.4
  preconditioning_steps           = 2
  stopping_criterion              = '||r||', 
  maximum_iterations              = 1000, 
/

&NUMERICS
  dt_init = 1.0d-03
  dt_min  = 1.0d-10
  dt_grow = 1.5
  dt_max  = 1.0
  discrete_ops_type = 'ortho'
/

&LEGACY_FLOW
  inviscid = .false.
  surface_tension = .true.
  projection_linear_solution = 'projection'
  viscous_implicitness       = 0.5
  courant_number             = 0.5
/

##### BOUNDARY CONDITIONS ######################################################

&THERMAL_BC
  name = 'cold surface'
  type = 'temperature'
  face_set_ids = 1, 2, 4
  temp = 1.0
/

&THERMAL_BC
  name = 'hot top'
  type = 'temperature'
  face_set_ids = 3
  temp = 2.0
/

&THERMAL_BC
  name = 'x-z symmetry'
  type = 'flux'
  face_set_ids = 5, 6
  flux = 0.0
/

&SURFACE_TENSION
  csf_boundary = .true.
  sigma_constant =  1.0 ! unused with csf, but avoids false error detection
  dsig_dT =  -1.0
  bndry_face_set_ids = 3
/

&BODY
  surface_name = 'from mesh file'
  mesh_material_number = 1
  material_name = 'liquid'
  temperature = 1
/

##### MATERIALS ################################################################

&MATERIAL
 name = 'liquid' 
 is_fluid = T
 density = 1.0
 specific_heat = 1.0
 conductivity = 1.0
 viscosity = 1.0
/
