The golden output for advection-old-2{b,c} are generated using the input files
advection-old-2{b,c}-ref.inp and NOT the test input files themselves. These
special input files are used to compute the exact VOFs for the exact final
material configuration for the tests, and used to to compare to the computed
final configurations. 
