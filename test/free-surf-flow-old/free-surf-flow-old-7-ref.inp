INVISCID FLUID PLUG ACCELERATING DOWN PIPE WITH VOID
----------------------------------------------------
This is a simple 1D inviscid fluid/void flow problem. The 3d 5x1x1 pipe domain is
void except for a plug of fluid near the left end but bordered by void on either
side. The fluid is subject to a gravity force directed down the pipe. It begins
with an initial velocity of 0 and accelerates to the right. At the final time
t=3 the plug should be near the right end (centered at x=1.5) and moving with
velocity 2.

This problem uses a regular Cartesian mesh whose node positions have been
perturbed by 10% of the edge length (and is therefore a non-orthogonal mesh).

We should recover the exact solution for pressure and velocity: P=0,
V=(1,0,0)*(2*t/3) on cells with fluid. The exact position of the fluid/void
interface should be x = -1 + t**2 / 3. We don't recover this exactly because
of the explicit advection scheme, but we use an unusually small courant number
here to reduce the error.

&MESH
  mesh_file = 'mesh4.g'
/

&OUTPUTS
  output_t  = 0.0, 3.0
  output_dt = 3.0
/

&PHYSICS
  materials = 'water', 'VOID'
  legacy_flow = .true.
  body_force_density = 0.6666666666666667, 0.0, 0.0
/

&NUMERICS
  dt_init = 0.01
  dt_grow = 1.05
  dt_min  = 1.0e-05
  dt_max  = 1.0
  cycle_max = 1
  discrete_ops_type = 'ortho'
/

&LEGACY_FLOW
  inviscid = .true.
  volume_track_interfaces = .true.
  volume_track_brents_method  = .true.
  volume_track_iter_tol = 1.0e-12
  volume_track_subcycles = 1
  projection_linear_solution = 'projection'
  body_force_face_method = .true.
  courant_number = 0.05
/

&LINEAR_SOLVER
  name = 'projection'
  method = 'fgmres'
  preconditioning_method = 'ssor'
  convergence_criterion  = 1.0e-10
  relaxation_parameter   = 1.4
  preconditioning_steps  = 2
  maximum_iterations     = 200
/

INFLOW
&BC
  surface_name = 'from mesh file'
  mesh_surface = 1
  !bc_variable  = 'velocity'
  !bc_type      = 'dirichlet'
  !bc_value     = 1.0, 0.0, 0.0
  !inflow_material = 'water'
  bc_variable  = 'pressure'
  bc_type      = 'dirichlet'
  bc_value     = 0.0
/

OUTFLOW
&BC
  surface_name = 'from mesh file'
  mesh_surface = 2
  bc_variable  = 'pressure'
  bc_type      = 'dirichlet'
  bc_value     = 0.0
/
 
Fluid plug
&BODY
  surface_name = 'box'
  length = 1.0, 1.0, 1.0
  translation_pt = 1.5, 0.0, 0.0
  material_name = 'water'
  velocity = 0.0, 0.0, 0.0
  temperature = 0.0
/

Surrounding void
&BODY
  surface_name = 'background'
  mesh_material_number = 2
  material_name = 'void'
  velocity = 0.0, 0.0, 0.0
  temperature = 0.0
/


&MATERIAL
  name = 'water'
  is_fluid = T
  density = 1.0
/
