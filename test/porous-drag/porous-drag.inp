
Pressure Driven Inviscid Flow with Porous Drag:
Constant Property, Othogonal Mesh

       Uni-directional flow

Basically, flow through a straight channel of width 2h

-----------------------------------------------------
       ^y           |---->             / \
       |            |---->              |
p1     |--->x       |---->  U           | 2h         p2 
                    |---->              |
                    |---->             \ / 
----------------------------------------------------

Half of the single cell in the z-direction is filled with fluid
    and the remaining half with solid

The Darcy Law states that 

gradP = -C(1-lambda)^2/lambda^3u

lambda is the porosity constant
C is the permeability constant
gradP is the presure gradient

The porous source terms are given by
Sy = -Av     Sz = -Aw

where A can be ascertained

A = C(1-lambda)^2/lambda^3
 
----------------------------------------------------------------------                     
&MESH

       Mesh_File = 'mesh.exo'

/
 
&OUTPUTS
 
       Output_T = 0.0, 0.500, 
       Output_Dt = 0.01,
       output_dt_Multiplier  =  1,

/

&LINEAR_SOLVER

      name                   = 'ssor-gmres',
      method                 = 'gmres',
      convergence_criterion  = 1.0e-8 
      preconditioning_method = 'ssor',
      maximum_iterations     = 200
      relaxation_parameter   = 1.4,
      preconditioning_steps  = 4,
      stopping_criterion     = '||r||/||x||'

/

&LINEAR_SOLVER

      name                   = 'projection',
      method                 = 'fgmres',
      convergence_criterion  = 1.0e-14,
      preconditioning_method = 'ssor',
      maximum_iterations     = 200
      relaxation_parameter   = 1.4,
      preconditioning_steps  = 4,
      stopping_criterion     = '||r||'

/


&LEGACY_FLOW
 
       inviscid = .true.
       porous_flow = .true.
       projection_linear_solution = 'projection'
       courant_number       = 0.4
       porous_implicitness  = 0.5
       permeability_constant = 1000.0, 0.0, 0.0

/

&NUMERICS

       dt_init              = 0.0001
       discrete_ops_type    = 'ortho' 

/

&PHYSICS
  materials = 'fluid', 'solid'
 
       legacy_flow = .true.
       body_force_density = 0.0, 0.0, 0.0,

/


Velocity BC: Free slip on front and back of box; z=0 and z=dx


Pressure  BC:  on x=0 face  


&BC

       Surface_Name       = 'from mesh file',
       Mesh_Surface       = 1,
       BC_Variable        = 'pressure',
       BC_Type            = 'dirichlet',
       BC_Value           =  20000.0,  

/

Pressure  BC:  on x=20.0 face  

&BC

       Surface_Name       = 'from mesh file',
       Mesh_Surface       = 2,
       BC_Variable        = 'pressure',
       BC_Type            = 'dirichlet',
       BC_Value           =  0.0,  


/


Velocity BC: No slip on y=0  and y=1 face

&BC

       Surface_Name    = 'from mesh file',
       Mesh_Surface    = 3,
       BC_Variable     = 'velocity',
       BC_Type         = 'no-slip',
       BC_Value        =    0.0,

/


&BC

       Surface_Name    = 'from mesh file',
       Mesh_Surface    = 4,
       BC_Variable     = 'velocity',
       BC_Type         = 'no-slip',
       BC_Value        =    0.0, 

/

&BODY
 
       Material_name   = 'fluid'
       Surface_Name    = 'plane',
       axis = 'z'
       Translation_Pt  = 10.0, 0.0, 0.5
       Velocity        = 0.0, 0.0, 0.0,
       Temperature     = 273.0

/
 
&BODY
 
       Material_name   = 'solid'
       Surface_Name    = 'background',
       Velocity        = 0.0, 0.0, 0.0,
       Temperature     = 273.0

/


&MATERIAL
  name = 'fluid'
  is_fluid = T
  density = 1.0
  specific_heat = 1.0
  viscosity = 1.0
/


&MATERIAL
  name = 'solid'
  density = 1.0
  specific_heat = 1.0
/



&PARALLEL_PARAMETERS

        Partitioner = 'Chaco',

/

&PROBE
  data_file          = 'downstream.dat'
  description        = 'downstream centerline'
  coord              = 15.0, 0.5, 0.5
  coord_scale_factor = 1.0
  data               = 'pressure'
/
